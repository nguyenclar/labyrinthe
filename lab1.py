
import sys
from fonctionlaby import*

d1 = saisieNombre('Choisissez entre les différentes versions:\n 1= Les murs du labyrinthe peuvent être isolés,\n 2= Les murs du labyrinthe ne peuvent pas être isolés: \n ')
if d1 ==1:
    d2 = saisieNombre("Choisissez de nouveau une version:\n 3= Vous pourrez voir toutes les solutions du labyrinthe possible,\n 4= Vous ne pourrez voir qu'une seule solution de résolution,\n 5= Le programme s'arrête lorsque le point B est atteint, \n 6= Le programme est résolution suivant le systeme de la main droite : \n ")
elif d1 ==2:
    d2 = saisieNombre ("Choisissez de nouveau une version:\n 3= Vous ne pourrez voir qu'une seule solution de résolution, \n 4= Le programme s'arrête lorsque le point B est atteint, \n 5= Le programme est résolution suivant le systeme de la main droite: \n")

# Nombre de lignes dans le labyrinthe.
n = saisieNombre('Nombre de lignes : ')
n = n * 2 + 1
# Nombre de colonnes dans le labyrinthe.
m = saisieNombre('Nombre de colonnes : ')
m = m * 2 + 1



#appel des fonctions

labyrinthe = creerLabyrinthe(n, m)



if d1==1:
    zero=False
    while zero==False:
            h=randomeh(n)
            l=randomel(m)
            bh=pair(h)
            bl=pair(l)
            #(x,y)= enlevmur(h,l,labyrinthe)
            (x,y)= enlevmur(h,l,labyrinthe)
            propagation(1,1,m,n,x,y,labyrinthe)
            zero=finrandom(1,1,m,n,labyrinthe)
    afficheLabyrinthe(labyrinthe)
    (ax,ay) = saisiePorte ("Entrez les lignes de la porte d'entrée: "," Entrez les colonnes de la porte d'entrée: "  ,m,n)
    (bx,by) = saisiePorte ("Entrez les lignes de la porte de sortie: "," Entrez les colonnes de la porte de sortie: "  ,m,n)
    labyrinthe[ax][ay]= (-2)
    labyrinthe[bx][by]= (-3)
    liste=[]
    (nax,nay)=nextporte(ax,ay,n,m)
    nexta=(nax,nay)
    nextb=nextporte(bx,by,n,m)
    labyrinthe[nax][nay]=1

    if d2 ==3:

        liste.append((nax,nay))
        chemin(labyrinthe,1,liste)
        liste.append(nextb)
        pluscourt(labyrinthe,nexta,liste)
        afficheLabyrinthe(labyrinthe)

    if d2 ==4:

        liste.append((nax,nay))
        chemin(labyrinthe,1,liste)
        liste.append(nextb)
        pluscourt2(labyrinthe,nexta,liste)
        afficheLabyrinthe(labyrinthe)


    if d2 ==5:

        listeturfu=[]
        liste.append(nexta)
        bol=False
        listeturfu.append(nextb)
        case=chemin2(labyrinthe,1,liste,bol,nextb,0)
        pluscourt(labyrinthe,nexta,listeturfu)
        afficheLabyrinthe(labyrinthe)
        print("Le nombre de cases explorées par l'algorithme est ", case," .")

    if d2 ==6:

        ((nax,nay),dep)=nextporte2(ax,ay,n,m)
        b=(bx,by)
        (nombre_pas,nombre_case)=maindroite(labyrinthe,nax,nay,dep,b,0,0)
        afficheLabyrinthe(labyrinthe)
        print("Le nombre de cases explorées par l'algorithme est ", nombre_case," .")
        print("Pour sortir du labyrinthe avec cette technique, la personne aurait fait ", nombre_pas," pas.")


if d1 == 2 :
    zero=False
    while zero==False:
            h=randomeh(n)
            l=randomel(m)
            bh=pair(h)
            bl=pair(l)
            (x,y)= enlevmur2(h,l,labyrinthe)
            propagation(1,1,m,n,x,y,labyrinthe)
            zero=finrandom(1,1,m,n,labyrinthe)
    afficheLabyrinthe(labyrinthe)
    (ax,ay) = saisiePorte ("Entrez les lignes de la porte d'entrée: "," Entrez les colonnes de la porte d'entrée: "  ,m,n)
    (bx,by) = saisiePorte ("Entrez les lignes de la porte de sortie: "," Entrez les colonnes de la porte de sortie: "  ,m,n)
    labyrinthe[ax][ay]= (-2)
    labyrinthe[bx][by]= (-3)
    liste=[]
    (nax,nay)=nextporte(ax,ay,n,m)
    nexta=(nax,nay)
    nextb=nextporte(bx,by,n,m)
    labyrinthe[nax][nay]=1

    if d2 == 3:

        liste.append((nax,nay))
        chemin(labyrinthe,1,liste)
        liste.append(nextb)
        pluscourt(labyrinthe,nexta,liste)
        afficheLabyrinthe(labyrinthe)

    if d2 ==4:

        listeturfu=[]
        liste.append(nexta)
        bol=False
        listeturfu.append(nextb)
        case=chemin2(labyrinthe,1,liste,bol,nextb,0)
        pluscourt(labyrinthe,nexta,listeturfu)
        afficheLabyrinthe(labyrinthe)
        print("Le nombre de cases explorées par l'algorithme est ", case," .")

    if d2 ==5:

        ((nax,nay),dep)=nextporte2(ax,ay,n,m)
        b=(bx,by)
        (nombre_pas,nombre_case)=maindroite(labyrinthe,nax,nay,dep,b,0,0)
        afficheLabyrinthe(labyrinthe)
        print("Le nombre de cases explorées par l'algorithme est ", nombre_case," .")
        print("Pour sortir du labyrinthe avec cette technique, la personne aurait fait ", nombre_pas," pas.")
