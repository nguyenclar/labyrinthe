#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

sys.setrecursionlimit(50000)
from random import randint
###
# Fonction demandant la saisie d’un nombre entier en s’assurant que
# l’utilisateur entre bien un nombre.
# Entrée : phrase d’invite.
# Retourne : le nombre saisi.
def saisieNombre (invite):
    # Nombre saisi.
    x = None
    while not x:
        try:
            x = int(input(invite))
        except ValueError:
            print('Entrée invalide.')
    return x

###
# Fonction créant le tableau de départ pour représenter un labyrinthe.
# Entrées : n : nombre de lignes.
#           m : nombre de colonnes.
# Sortie : un tableau pour représenter le labyrinthe.
def creerLabyrinthe (n, m):
    # Tableau contenant un labyrinthe.
    labyrinthe = [[-1] * m for _ in range(n)]

    # Compteur du nombre de cases.
    compteur = 0
    for i in range(1, n - 1, 2):
        for j in range(1, m - 1, 2):
            labyrinthe[i][j] = compteur
            compteur += 1
    print (compteur)
    return labyrinthe

###
# Procédure affichant le labyrinthe.
# Entrées : labyrinthe : tableau contenant le labyrinthe.
def afficheLabyrinthe (labyrinthe):
    for ligne in labyrinthe:
        for col in ligne:
            if col == -1:
                print(' * ', sep = "", end = "")
            elif col==0:
                print('   ',sep = "", end = "")
            elif col==-2:
                print(' A ',sep = "", end = "")
            elif col==-3:
                print(' B ',sep = "", end = "")
            elif col==-4:
                print(':::',sep = "", end = "")
            else:
                print('   ', sep = "", end = "")
        print('')

def afficheLabyrinthe2 (labyrinthe):
    for ligne in labyrinthe:
        for col in ligne:
            if col == -1:
                print(' * ', sep = "", end = "")
            elif col==0:
                print('   ',sep = "", end = "")
            elif col==-2:
                print(' A ',sep = "", end = "")
            elif col==-3:
                print(' B ',sep = "", end = "")
            elif col==-4:
                print(':::',sep = "", end = "")
            else:
                print('{:03d}'.format(col), sep = "", end = "")
        print('')
###
# Fonction définissant un nombre pair ou impair.
# Entrées : nombre : nombre entré par l'utilisateur.
# Sortie : un booléen disant si c'est pair ou impair.
def pair (nombre):
    if nombre%2==0:
        paire=True
    else:
        paire=False
    return paire

###
# Fonction enlève les murs du labyrinthe (parcours le labyrinthe, regarde les valeurs en haut et en bas de la case séléctionnée aléatoirement et remplace par la plus petite valeur).
# Entrées : h: coordonné (de la hauteur) random.
#           l: coordonné (de la longueur) random
#           lab: tableau du labyrinthe
# Sortie : maximum et minimum
def enlevmur(h,l,lab):
    if (pair(h)==True) and (pair(l)==False):
        x=lab[h+1][l]
        y=lab[h-1][l]
        max_val=max(x,y)
        min_val=min(x,y)
        lab[h][l]=min_val
        return(max_val,min_val)
    if (pair(h)==False) and (pair(l)==True):
        x=lab[h][l+1]
        y=lab[h][l-1]
        max_val=max(x,y)
        min_val=min(x,y)
        lab[h][l]=min_val
        return(max_val,min_val)
    else :
        return('a','b')

def enlevmur2(h,l,lab):
    if (pair(h)==True) and (pair(l)==False):
        x=lab[h+1][l]
        y=lab[h-1][l]
        if x!=y:
            max_val=max(x,y)
            min_val=min(x,y)
            lab[h][l]=min_val
            return(max_val,min_val)
        else:
            return('a','b')
    if (pair(h)==False) and (pair(l)==True):
        x=lab[h][l+1]
        y=lab[h][l-1]
        if x!=y:
            max_val=max(x,y)
            min_val=min(x,y)
            lab[h][l]=min_val
            return(max_val,min_val)
        else:
            return('a','b')
    else :
        return('a','b')

###
# Fonction qui choix valeur aléatoire
# Entrées : ligne: nombre de lignes.
# Sortie : valeur choisi aléatoirement dans les lignes.
def randomeh(ligne):
    return randint(1,ligne-2)

###
# Fonction qui choix valeur aléatoire
# Entrées : colonne: nombre de colonnes.
# Sortie : valeur choisi aléatoirement dans les colonnes.
def randomel(colonne):
    return randint(1,colonne-2)

###
# Fonction qui arrête de supprimer les murs du labyrinthe récursivement
# Entrées : i : coordonné de la hauteur
#           j : coordonné de la longueur
#           n : nombre de lignes.
#           m : nombre de colonnes.
#           labyrinthe: tableau representant le labyrinthe
# Sortie : booléen pour savoir si il reste des 0 sur les cases impaires impaires
def finrandom(i,j,m,n,labyrinthe):
    if  labyrinthe[i][j]!=0:
        return (False)
    if labyrinthe[i][j]==0 and i<(n-2) and j<=(m-2):
            return (finrandom(i+2,j,m,n,labyrinthe))
    if labyrinthe[i][j]==0 and i==(n-2) and j<(m-2):
            return (finrandom(1,j+2,m,n,labyrinthe))
    if labyrinthe[i][j]==0 and i==(n-2) and j==(m-2):
            return (True)
###
#Procédure qui propage la plus petite valeur dans le tableau
#Entrées :i : coordonné de la hauteur
#           j : coordonné de la longueur
#           n : nombre de lignes.
#           m : nombre de colonnes.
#           maxi : le maximum des deux valeurs entourant le mur surprimé précédement
#           mini : le minimum des deux valeurs entourant le mur surprimé précédement
#           lab: tableau representant le labyrinthe
def propagation (i,j,m,n,maxi,mini,lab):
    if i!=n-1 or j!=m-1:
        if i!=n-1 and j!=m-1:
            if lab[i][j]==maxi:
                lab[i][j]=mini
                propagation(i+1,j,m,n,maxi,mini,lab)
            else:
                propagation(i+1,j,m,n,maxi,mini,lab)
            #afficheLabyrinthe(labyrinthe)
        elif i==n-1 and j!=m-1:
            if lab[i][j]==maxi:
                lab[i][j]=mini
                propagation(1,j+1,m,n,maxi,mini,lab)
            else:
                propagation(1,j+1,m,n,maxi,mini,lab)
            #afficheLabyrinthe(labyrinthe)

###
# Fonction demandant la saisie d’un coordonné (nombre entier) en s’assurant que l’utilisateur entre bien un nombre.
# Entrée : invitex: phrase d’invite pour le coordonné x.
#          invitey: phrase d’invite pour le coordonné y.
#           n : nombre de lignes.
#           m : nombre de colonnes.
# Retourne : les nombres saisis.
def saisiePorte (invitex, invitey, m,n):
    # Nombre saisi.
    x = 0
    y = 0
    while (not x and not y) or porteDepart(m,n,x,y)== False:
        try:
            x = int(input(invitex))
            y = int(input(invitey))
        except ValueError:
            print('Entrée invalide.')
    return (x,y)

###
# Fonction qui verifie les conditions pour placer une porte
# Entrée: n : nombre de lignes.
#         m : nombre de colonnes.
#         cox : coordonné rentré par l'utlisateur pour les lignes.
#         coy : coordonné rentré par l'utlisateur pour les colonnes.
# Retourne : booléen pour savoir si la pourte peut être placée ou non.
def porteDepart (m,n,cox,coy):
    if ((pair(cox)==False) and ((pair(coy)==False)) or ((pair(cox)==True) and (pair(coy)==True))):
        print ("Ce n'est pas une bordure, réessayez, ")
        return False
    if ((cox>0 and cox< (n-1)) and (coy == 0 or coy == (m-1))) or ((cox ==0 or cox == (n-1)) and (coy< (m-1) and coy>0)):
        return True
    else:
        print ("Ce n'est pas une bordure, réessayez, ",n ,m,cox,coy)
        return False

###
# Fonction permmettant de savoir si on a atteint la porte de sorti afin d'arreter de compter les cases du chemin
# Entrée : liste : liste contenant des coordonnées
#          nb : les coordonnées de la case en face de la porte de sortie
# Sortie : Booléen pour savoir si on a atteint la porte de sortie
def compare(liste,nb):
    if liste!=[]:
    #for j in range (len(liste)):
        (h,l)=liste[0]
        if (h,l)!=nb:
            return False
        else:
            return True
    else:
        return False
###
# Procédure crée les chemins possibles dans la labyrinthe
# Entrée: lab : tableau du labyrinthe
#         i : variable pour incrémenter
#         liste : liste pour stocker valeurs
def chemin(lab,i,liste):
    if liste!=[]:
        liste2=[]
        while liste!=[]:
            (h,l)=liste.pop(0)
            if lab[h+1][l]==0:
                lab[h+1][l]=i+1
                liste2.append((h+1,l))
            if lab[h-1][l]==0:
                lab[h-1][l]=i+1
                liste2.append((h-1,l))
            if lab[h][l+1]==0:
                lab[h][l+1]=i+1
                liste2.append((h,l+1))
            if lab[h][l-1]==0:
                lab[h][l-1]=i+1
                liste2.append((h,l-1))
            #print(liste2)
            #print(liste)
            #afficheLabyrinthe(laby)
        chemin(lab,i+1,liste2)

###
# Fonction qui crée les chemins possibles dans la labyrinthe
# Entrée: lab : tableau du labyrinthe
#         i : variable pour incrémenter
#         liste : liste pour stocker valeurs
# Sortie: le nombre de cases parcourues
def chemin2(lab,i,liste,boool,nB,nbcase):
    if boool==False:
        liste2=[]
        while (liste!=[]) and (boool==False):
            boool=compare(liste,nB)
            nbcase=nbcase+1
            (h,l)=liste.pop(0)
            if lab[h+1][l]==0:
                lab[h+1][l]=i+1
                liste2.append((h+1,l))
            if lab[h-1][l]==0:
                lab[h-1][l]=i+1
                liste2.append((h-1,l))
            if lab[h][l+1]==0:
                lab[h][l+1]=i+1
                liste2.append((h,l+1))
            if lab[h][l-1]==0:
                lab[h][l-1]=i+1
                liste2.append((h,l-1))
        return chemin2(lab,i+1,liste2,boool,nB,nbcase)

    else:
        return nbcase
###
# Procédure qui cherche le plus court chemin le plus court chemin
# Entrée: lab : tableau du labyrinthe
#         A : coordonnés x et y
#         liste : liste pou stocker valeurs
def pluscourt(lab,A,liste):
    (x,y)=A

    if lab[x][y]!=-4:
        liste2=[]
        while liste!=[]:
            (h,l)=liste.pop(0)
            if lab[h+1][l]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h+1,l))
            if lab[h-1][l]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h-1,l))
            if lab[h][l+1]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h,l+1))
            if lab[h][l-1]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h,l-1))
            lab[h][l]=-4
        pluscourt(lab,A,liste2)

def pluscourt2(lab,A,liste):
    (x,y)=A

    if lab[x][y]!=-4:
            liste2=[]
            #while liste!=[]:
            (h,l)=liste.pop(0)
            if lab[h+1][l]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h+1,l))
            if lab[h-1][l]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h-1,l))
            if lab[h][l+1]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h,l+1))
            if lab[h][l-1]==(lab[h][l]-1):
                #lab[h][l]=-4
                liste2.append((h,l-1))
            lab[h][l]=-4
            pluscourt2(lab,A,liste2)


###
# Fonction permmettant de savoir le nombre de case parcouru par la fonction
# Entrée : lab : le tableau contenant le labyrinthe
#          caze : le nombre de case parcouru
#          x : le numéro de la ligne
#          y : le numéro de la colonne
# Sortie : le nombre de case parcouru
def caseparcouru(lab,caze,x,y):
    if lab[x][y]==0:
        caze=caze+1
    return caze

###
# Fonctions permettant de faire tourner main droite en fonction de la direction
# Entrée : lab : le tableau contenant le labyrinthe
#          x : le numéro de la ligne
#          y : le numéro de la colonne
# Sortie : nouvelle coordonnée pour x et y, ainsi q'une lettre permettant de faire les instructions suivantes
def fonctiona(lab,x,y):
    if (lab[x][y-1]==-1 and lab[x+1][y]!=-1):
        return (x+1,y,"a")
    elif (lab[x][y-1]==-1 and lab[x+1][y]==-1):
        return (x,y,"d")
    else:
        return (x,y-1,"b")

def fonctionb(lab,x,y):
    if lab[x-1][y]==-1 and lab[x][y-1]!=-1:
        return (x,y-1,"b")
    elif lab[x-1][y]==-1 and lab[x][y-1]==-1:
        return (x,y,"a")
    else:
        return (x-1,y,"c")

def fonctionc(lab,x,y):
    if lab[x][y+1]==-1 and lab[x-1][y]!=-1:
        return (x-1,y,"c")
    elif lab[x][y+1]==-1 and lab[x-1][y]==-1:
        return (x,y,"b")
    else:
        return (x,y+1,"d")

def fonctiond(lab,x,y):
    if lab[x+1][y]==-1 and lab[x][y+1]!=-1:
        return (x,y+1,"d")
    elif lab[x+1][y]==-1 and lab[x][y+1]==-1:
        return (x,y,"c")
    else:
        return (x+1,y,"a")
###
# Fonction permettant de stimuler la résolution d'un labyrinthe avec la méthode de la main droite
# Entrée :  lab : le tableau contenant le labyrinthe
#          x : le numéro de la ligne
#          y : le numéro de la colonne
#          boucle : lettre dirigeant les insctructions qui vont suivre
#          nB : les coordonnées de la porte de sortie du labyrinthe
#          pas : le nombre de pas fait par une personne dans le labyrinthe
#          case : le nombre de case parcouru par la fonction
# Sortie : le nombre de pas et le nombre de case parcouru
def maindroite(lab,x,y,boucle,nB,pas,case):
  if (x,y)!=nB:
    case=caseparcouru(lab,case,x,y)
    lab[x][y]=-4
    if boucle=="a":
        (x,y,boucle)=fonctiona(lab,x,y)
        return maindroite(lab,x,y,boucle,nB,pas+1,case)
    if boucle=="b":
        (x,y,boucle)=fonctionb(lab,x,y)
        return maindroite(lab,x,y,boucle,nB,pas+1,case)
    if boucle=="c":
        (x,y,boucle)=fonctionc(lab,x,y)
        return maindroite(lab,x,y,boucle,nB,pas+1,case)
    if boucle=="d":
        (x,y,boucle)=fonctiond(lab,x,y)
        return maindroite(lab,x,y,boucle,nB,pas+1,case)
  else:
      return (pas,case)

###
# Fonction qui détermine les coordonée de la case en face d'une porte et un depart pour l'algorithme maindroite
# Entrée: x : coordonné de la porte
#         y : coordonné de la porte
# Retourne : le coordonné dans le labyrinthe le plus proche de la porte

def nextporte(x,y,n,m):
    if x==0:
        nexti=(x+1,y)
    if y==0:
        nexti=(x,y+1)
    if x==(n-1):
        nexti=(x-1,y)
    if y==(m-1):
        nexti=(x,y-1)
    return nexti

def nextporte2(x,y,n,m):
    if x==0:
        depart="a"
        nexti=(x+1,y)
    if y==0:
        depart="d"
        nexti=(x,y+1)
    if x==(n-1):
        depart="c"
        nexti=(x-1,y)
    if y==(m-1):
        depart="b"
        nexti=(x,y-1)
    return (nexti,depart)
